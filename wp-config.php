<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'hta');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'V%:!?)(qdi=C ]/)Atfuml311lcI.zJS[<4fmq!|8, b4vsaaneIA<egt=Vxw`-T');
define('SECURE_AUTH_KEY',  'CBeoM0b6i*dSAB;Z#^lb-__t#/[lX1l^ fudz`$*1TbSd9~x[KsqVP}Y%Yi<|hzX');
define('LOGGED_IN_KEY',    'v}:CTY~b:netc,jD{IYMjuC)l:CggWnqcGWubPb{2|>6]Mw{3.-T7{U8<SF?SuGJ');
define('NONCE_KEY',        '4*|2Yer}j dX^@`~:7i=.HmPZaZaq(#tioqh(wD@Pj ea{D+(_b1es!E/7OS!|pR');
define('AUTH_SALT',        '7A]ke~bbp/K=j]zdyoG9U`y_`)Jg`TrBz8{gx9/jBCVVK8bhNaXu+-(G>0+VdHUq');
define('SECURE_AUTH_SALT', '#n-),;r#l>X#^FaWy%gD24@quYe}<F732YX<*a?EwPhV6W`PZUs{.r:7Qdrl`Hw_');
define('LOGGED_IN_SALT',   '$9;Y;z,u$1=+qKbu&6d8fZX|Le*>k)FpKvSoSQ6[a^VZ*_v&32*::)mk*a--P_TD');
define('NONCE_SALT',       'f&v{O+s.hAIS[q2p$uoa}M3qPsV1-I(*>&W^j#K6Ppup8b uQ,ET4jX<em8nPxNW');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
